var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Goods = require("../models/goods");

mongoose.connect("mongodb://127.0.0.1:27017/dumall");
mongoose.connection.on("connected",()=>{
	console.log("mongodb connected success..");
});
mongoose.connection.on("error",()=>{
	console.log("mongodb connected error");
});
mongoose.connection.on("disconnected",()=>{
	console.log("mongodb connected disconnected");
});
router.get("/list",(req,res,next)=>{
	let page = req.param("page");
	let pageSize =parseInt(req.param("pageSize"));
	let priceLevel = req.param("priceLevel");
	let sort = req.param("sort");
	var priceGt,priceLte;
	let skip = (page-1)*pageSize; 
	let params = {};
	if(priceLevel != 'all'){
		switch(priceLevel){
			case "0":priceGt = 0;priceLte = 100;break;
			case "1":priceGt = 100;priceLte = 500;break;
			case "2":priceGt = 500;priceLte = 1000;break;
			case "3":priceGt = 1000;priceLte = 5000;break;
		}
		params = {
			salePrice:{
				$gt:priceGt,
				$lte:priceLte
			}
		}
	}
	
	
	let goodModel = Goods.find(params).skip(skip).limit(pageSize);
	goodModel.sort({'salePrice':sort});
	goodModel.exec((error,doc)=>{
		if(error){
			res.json({
				status:"0",
				msg:error.message
			})
		}else{
			
			res.json({
				status:"1",
				msg:'',
				result:{
					count:doc.length,
					list:doc
				}
			})
		}
	})
});
router.post("/addCart",(req,res,next)=>{
	let userId = '100000077';
	let productId = req.body.productId;
	let User = require("../models/users");
	User.findOne({userId:userId},(error,userDoc)=>{
		if(error){
			res.json({
				status:"0",
				msg:error.message
			})
		}else{
			if(userDoc){
				let goodsItem = '';
				userDoc.cartList.forEach((item)=>{
					if(item.productId == productId){
						goodsItem = item;
						item.productNum++;
					}else{

					}
				});
				if(goodsItem){
					userDoc.save((err1,doc2)=>{
								
								if(err1){
									res.json({
										status:"0",
										msg:err1.message
									})
								}else{
									res.json({
										status:"1",
										msg:'',
										result:doc2
									})
								}
								
							});
				}else{
					Goods.findOne({productId:productId},(err,doc)=>{
						if(err){
							res.json({
								status:"0",
								msg:err.message
							})
						}else{
							if(doc){
								doc.productNum = 1;
								doc.checked = 1; 

								userDoc.cartList.push(doc);
								userDoc.save((err1,doc2)=>{
									
									if(err1){
										res.json({
											status:"0",
											msg:err1.message
										})
									}else{
										res.json({
											status:"1",
											msg:'',
											result:doc2
										})
									}
									
								});
							}
						}
					})
				}
				
			}
		}
	})
})
module.exports=router;