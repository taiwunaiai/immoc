var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var productSchema = new Schema({
	productId:String,
	productName:String,
	productImage:String,
	salePrice:Object,
	productNum:Number,
	checked:String,
})
module.exports = mongoose.model("good",productSchema);