// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import resource from 'vue-resource';
import VueLazyLoad from 'vue-lazyload' ;
import InfiniteScroll from 'vue-infinite-scroll' ;
import Vuex from 'vuex' ;
Vue.config.productionTip = false;
Vue.use(InfiniteScroll);
Vue.use(VueLazyLoad,{
  laoding:'/static/laoding-svg/laoding-bars.svg',
  try:3,
});
Vue.use(Vuex);
/* eslint-disable no-new */
const store = new Vuex.Store({
  state:{
    nickName:'',
    cartCount:0,
  },
  mutations:{
    updateUserInfo(state, nickName){
      state.nickName = nickName;
    },
    updateCartCount(state, cartCount){
      state.cartCount += cartCount;
    },
    initCartCount(state,cartCount){
      state.cartCount = cartCount;
    }
  }
})
new Vue({
  router,
  template: '<App/>',
  store,
  components: { App }
}).$mount("#app");
